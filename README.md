# mersivity

## Description
Mersivity is immersive and submersive and just-plain "mersive"

## Authors and acknowledgment
https://gitlab.com/di.lei<br>
https://gitlab.com/ptoone<br>
https://gitlab.com/santiago.arciniegas<br>
https://gitlab.com/soberdavid<br>
https://gitlab.com/SteveMann<br>
## License
This project follows a Copyleft approach with software licensed under a GNU General Public License v3.0

## Project status
In development. 
